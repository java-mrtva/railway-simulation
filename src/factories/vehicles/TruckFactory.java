package factories.vehicles;

import models.dynamical.vehicles.Truck;
import models.statical.Road;
import models.statical.RoadLane;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class TruckFactory {
    private static final Map<String, List<String>> BRANDS = Map.of(
            "FAP", List.of(
                    "1314",
                    "1318",
                    "2228"
            ),
            "TAM", List.of(
                    "Pionir",
                    "4500",
                    "5000"
            ),
            "MAN", List.of(
                    "TGL",
                    "eTGM",
                    "TGX"
            )
    );

    public static Truck generate(Road road) {
        RoadLane roadLane = road.getRoadLanes().get(new Random().nextInt(road.getRoadLanes().size()));
        String brand = (String) BRANDS.keySet().toArray()[(int) (new Random().nextInt(BRANDS.size()))];
        List<String> models = BRANDS.get(brand);
        String model = models.get(new Random().nextInt(models.size()));
        int year = new Random().nextInt(50) + 1960;
        int speed = road.getSpeedLimit() + new Random().nextInt(200);
        int capacity = new Random().nextInt(10) + 1;
        return new Truck(
                roadLane,
                speed,
                brand,
                model,
                year,
                capacity
        );
    }
}
