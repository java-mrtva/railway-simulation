#Train; name; speed; startStationName; endStationName;
#Locomotive; label; locomotiveType: {PASSENGER, FREIGHT, UNIVERSAL, MANEUVER}; driveType: {STEAM, DIESEL, ELECTRIC}; power
#SeatAndBedWagon; label; length; numberOfSeats; numberOfBeds
#RestaurantWagon; label; length; description
#CargoWagon; label; length; capacity
#SpecialWagon; label; length;

Train;Voz1;100;A;E;
Locomotive;Lokomotiva1;FREIGHT;DIESEL;250;
Locomotive;Lokomotiva1;UNIVERSAL;DIESEL;250;
CargoWagon;Vagon1;100;100;50;
#RestaurantWagon;Vagon2;140;SUR Orao;
#SpecialWagon;Vagon2;300;
