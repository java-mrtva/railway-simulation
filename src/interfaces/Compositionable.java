package interfaces;

import ui_components.Cell;
import models.Position;

public interface Compositionable {
    void setPosition(Position position);

    Position getPosition();

    Cell getNewCell();

    String getLabel();
}
