package exceptions;

public class UnprocessableConfigurationException extends Exception {
    public UnprocessableConfigurationException() {
        super("Bad configuration");
    }

    public UnprocessableConfigurationException(String message) {
        super(message);
    }

}
