package watchers;

import exceptions.UnprocessableConfigurationException;
import models.Simulation;
import util.PathList;

import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.stream.Collectors;

public class ConfigurationWatcher extends AbstractWatcher {
    public ConfigurationWatcher(Simulation simulation) throws IOException {
        super(simulation, PathList.CONFIGURATION_DIRECTORY, StandardWatchEventKinds.ENTRY_MODIFY);
    }

    @Override
    public void handle(Path fileName) throws IOException, UnprocessableConfigurationException {
        try {
            List<String> content = Files.readAllLines(this.directory.resolve(fileName)).
                    stream().filter(line -> line.startsWith("ROAD")).collect(Collectors.toList());
            if (0 == content.size()) {
                throw new UnprocessableConfigurationException("Roads configuration missing");
            }
            for (String s : content) {
                String[] parameters = s.split(";");
                String name = parameters[0];
                int speedLimit = Integer.parseInt(parameters[1]);
                int numberOfVehicles = Integer.parseInt(parameters[2]);
                this.simulation.getRoads().stream()
                        .filter(road -> road.getName().startsWith(name))
                        .forEach(road -> {
                            road.setSpeedLimit(speedLimit);
                            road.putVehiclesToQueue(numberOfVehicles);
                        });
            }
        } catch (UnprocessableConfigurationException unprocessableConfigurationException) {
            throw unprocessableConfigurationException;
        } catch (Exception exception) {
            throw new UnprocessableConfigurationException("Bad configuration file format");
        }
    }
}