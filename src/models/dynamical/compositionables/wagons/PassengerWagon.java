package models.dynamical.compositionables.wagons;

import models.dynamical.Train;
import models.dynamical.compositionables.Wagon;

public abstract class PassengerWagon extends Wagon {
    public PassengerWagon(Train train, String label, double length) {
        super(train, label, length);
    }
}
