package models.dynamical.compositionables.wagons;

import ui_components.Cell;
import ui_components.ImageCell;
import models.dynamical.Train;
import models.dynamical.compositionables.Wagon;
import resources.Images;

public class CargoWagon extends Wagon {
    private double capacity;

    public CargoWagon(Train train, String label, double length, double capacity) {
        super(train, label, length);
        this.capacity = capacity;
    }

    @Override
    public Cell getNewCell() {
        return new ImageCell(Images.CARGO_WAGON);
    }
}
