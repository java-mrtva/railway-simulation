package models.dynamical.vehicles;

import ui_components.Cell;
import ui_components.ImageCell;
import models.dynamical.Vehicle;
import models.statical.RoadLane;
import resources.Images;

public class Car extends Vehicle {
    private int numberOfDoors;

    public Car(RoadLane roadLane, int speed, String brand, String model, int year, int numberOfDoors) {
        super(roadLane, speed, brand, model, year);
        this.numberOfDoors = numberOfDoors;
    }

    public Cell getNewCell() {
        return new ImageCell(Images.CAR);
    }

    @Override
    public String toString() {
        return "Car{" +
                ", roadLane=" + roadLane +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", speed=" + speed +
                "numberOfDoors=" + numberOfDoors +
                '}';
    }
}
