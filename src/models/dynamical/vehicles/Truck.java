package models.dynamical.vehicles;

import ui_components.Cell;
import ui_components.ImageCell;
import models.dynamical.Vehicle;
import models.statical.RoadLane;
import resources.Images;

public class Truck extends Vehicle {
    private double capacity;

    public Truck(RoadLane roadLane, int speed, String brand, String model, int year, int capacity) {
        super(roadLane, speed, brand, model, year);
        this.capacity = capacity;
    }

    public Cell getNewCell() {
        return new ImageCell(Images.TRUCK);
    }

    @Override
    public String toString() {
        return "Truck{" +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", speed=" + speed +
                "capacity=" + capacity +
                '}';
    }
}
