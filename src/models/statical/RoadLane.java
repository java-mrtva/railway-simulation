package models.statical;

import models.Position;
import models.Simulation;
import models.dynamical.Vehicle;
import resources.Colors;
import util.Trajectories;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;

import ui_components.*;
import interfaces.Renderable;

public class RoadLane implements Renderable, Runnable {
    private String name;
    private Road road;
    private final Queue<Vehicle> waitingVehicles;

    private final CopyOnWriteArrayList<Position> trajectory;

    public RoadLane(String name) throws Exception {
        if (!Trajectories.ROAD_LANES.containsKey(name)) {
            throw new Exception("Road lane does not exists");
        }
        this.name = name;
        this.trajectory = new CopyOnWriteArrayList<>(Trajectories.ROAD_LANES.getOrDefault(this.name, new Position[]{}));
        if (this.name.contains("LEFT")) {
            Collections.reverse(this.trajectory);
        }
        this.trajectory.forEach((position) -> position.setCell(getNewCell()));
        this.waitingVehicles = new LinkedList<>();
    }

    public Cell getNewCell() {
        return new ColorizedCell(Colors.ROAD_COLOR);
    }

    public Cell getNewCell(Position position) {
        if (this.road.isPositionRailwayCrossing(position)) {
            return this.road.getRailwayCrossing().getNewCell();
        }
        return this.getNewCell();
    }

    public Position getNextPosition(Position position) {
        int nextIndex = this.trajectory.indexOf(position) + 1;
        if (nextIndex == this.trajectory.size()) {
            return null;
        }

        return this.trajectory.get(nextIndex);
    }

    @Override
    public void Render() {
        this.trajectory.forEach(Position::Render);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoad(Road road) {
        this.road = road;
    }

    public Road getRoad() {
        return this.road;
    }

    public void addVehicleToQueue(Vehicle vehicle) {
        this.waitingVehicles.add(vehicle);
    }

    @Override
    public void run() {
        Position firstPosition = this.getNextPosition(null);
        while (Simulation.RUNNING) {
            if (!(firstPosition.getCell() instanceof ImageCell) && !firstPosition.isLocked()) {
                new Thread(this.waitingVehicles.poll()).start();
            }
        }
    }
}
