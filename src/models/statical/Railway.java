package models.statical;

import ui_components.Cell;
import ui_components.ColorizedCell;
import interfaces.Renderable;
import javafx.util.Pair;
import models.Position;
import models.Simulation;
import models.dynamical.Train;
import resources.Colors;
import util.Trajectories;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Railway implements Runnable, Renderable {
    private String name;
    private final CopyOnWriteArrayList<Position> trajectory;
    private final CopyOnWriteArrayList<Position> inverseTrajectory;
    private final Queue<Pair<Train, Boolean>> waitingTrains;

    private final CopyOnWriteArrayList<Train> currentTrains;
    private boolean areCurrentRidesInverse = false;

    private final TrainStation trainStationFrom;
    private final TrainStation trainStationTo;
    private RailwayCrossing railwayCrossing;

    public Railway(String name, TrainStation trainStationFrom, TrainStation trainStationTo) {
        this.trainStationFrom = trainStationFrom;
        this.trainStationTo = trainStationTo;

        this.name = name;
        this.trajectory = new CopyOnWriteArrayList<>(Trajectories.RAILWAYS.getOrDefault(this.name, new Position[]{}));
        this.trajectory.forEach((coordinate) -> coordinate.setCell(this.getNewCell()));
        this.inverseTrajectory = new CopyOnWriteArrayList<>(new ArrayList<>(this.trajectory));
        Collections.reverse(this.inverseTrajectory);

        this.currentTrains = new CopyOnWriteArrayList<>();
        this.waitingTrains = new LinkedList<>();
    }

    public Cell getNewCell() {
        return new ColorizedCell(Colors.RAILWAY_COLOR);
    }

    public Cell getNewCell(Position position) {
        if (this.isPositionRailwayCrossing(position)) {
            return this.railwayCrossing.getNewCell();
        }
        return this.getNewCell();
    }

    public boolean isPositionRailwayCrossing(Position position) {
        if(null != this.railwayCrossing) {
            for (Position p : this.railwayCrossing.getTrajectory()) {
                if (p.equals(position)) {
                    return true;
                }
            }
        }

        return false;
    }

    public void putTrainToQueue(Train train, TrainStation trainStation) {
        boolean isInverse = this.trainStationTo == trainStation;
        this.waitingTrains.add(new Pair<>(train, isInverse));
    }

    @Override
    public void Render() {
        this.trajectory.forEach(Position::Render);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TrainStation getTrainStationFrom() {
        return trainStationFrom;
    }

    public TrainStation getTrainStationTo() {
        return trainStationTo;
    }

    public CopyOnWriteArrayList<Position> getTrajectory() {
        return this.areCurrentRidesInverse ? this.inverseTrajectory : this.trajectory;
    }

    public Position getNextPosition(Position position) {
        CopyOnWriteArrayList<Position> trajectory = this.areCurrentRidesInverse ? this.inverseTrajectory : this.trajectory;
        Position nextPosition;
        if (null == position) {
            nextPosition = trajectory.get(0);
        }
        else {

            int nextIndex = trajectory.indexOf(position) + 1;
            if (nextIndex == trajectory.size()) {
                return null;
            }

            nextPosition = trajectory.get(nextIndex);
        }

        return nextPosition;
    }

    @Override
    public void run() {
        while (Simulation.RUNNING) {
            try {
                Thread.sleep(500);
            } catch (Exception exception) {
                Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
            }
            Pair<Train, Boolean> nextRide;
            Train nextTrain;
            Boolean isNextRideInverse;
            if (this.waitingTrains.isEmpty()) {
                continue;
            }
            if (!this.currentTrains.isEmpty()) {
                nextRide = this.waitingTrains.stream().filter((pair) -> pair.getValue() == this.areCurrentRidesInverse).findFirst().orElse(null);
            } else {
                nextRide = this.waitingTrains.peek();
            }
            if (null != nextRide) {
                this.waitingTrains.remove(nextRide);
                nextTrain = nextRide.getKey();
                isNextRideInverse = nextRide.getValue();
                nextTrain.setCurrentRailway(this);
                this.currentTrains.add(nextTrain);
                this.areCurrentRidesInverse = isNextRideInverse;

                new Thread(nextTrain).start();
            }
        }
    }

    public void arrivedOnTheNextStation(Train train) {
        TrainStation trainStation = this.areCurrentRidesInverse ? trainStationFrom : trainStationTo;
        trainStation.redirectTrain(train);
        this.currentTrains.remove(train);
        if (this.currentTrains.isEmpty()) {
            this.areCurrentRidesInverse = false;
        }
    }

    public void setRailwayCrossing(RailwayCrossing railwayCrossing) {
        this.railwayCrossing = railwayCrossing;
    }

    public CopyOnWriteArrayList<Train> getCurrentTrains() {
        return currentTrains;
    }

    public boolean areCurrentRidesInverse() {
        return this.areCurrentRidesInverse;
    }
}

