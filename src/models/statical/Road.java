package models.statical;

import factories.VehicleFactory;
import interfaces.Renderable;
import models.Position;
import models.Simulation;
import models.dynamical.Vehicle;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Road implements Renderable, Runnable {
    private final String name;
    private int speedLimit;
    private int numberOfVehicles = 0;
    private final Queue<Vehicle> waitingVehicles;

    private final CopyOnWriteArrayList<RoadLane> roadLanes;
    private RailwayCrossing railwayCrossing;

    public Road(String name) {
        this.name = name;
        this.roadLanes = new CopyOnWriteArrayList<>();
        this.waitingVehicles = new LinkedList<>();
        for (String road_name : List.of("_LEFT", "_RIGHT")) {
            try {
                RoadLane roadLane = new RoadLane(this.name + road_name);
                roadLane.setRoad(this);
                this.roadLanes.add(roadLane);
            } catch (Exception exception) {
                Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
            }
        }
    }

    @Override
    public void Render() {
        this.roadLanes.forEach(RoadLane::Render);
    }

    public synchronized void putVehiclesToQueue(int numberOfVehicles) {
        if (this.numberOfVehicles < numberOfVehicles) {
            int newVehiclesCount;
            newVehiclesCount = numberOfVehicles - this.numberOfVehicles;
            this.numberOfVehicles = numberOfVehicles;
            for (int i = 0; i < newVehiclesCount; i++) {
                this.waitingVehicles.add(VehicleFactory.generate(this));
            }
        }
    }

    @Override
    public void run() {
        this.roadLanes.forEach(roadLane -> new Thread(roadLane).start());
        while (Simulation.RUNNING) {
            try {
                Thread.sleep(2000);
            } catch (Exception exception) {
                Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
            }

            if (this.waitingVehicles.isEmpty()) {
                continue;
            }

            Vehicle nextVehicle = this.waitingVehicles.poll();
            nextVehicle.getRoadLane().addVehicleToQueue(nextVehicle);
        }
    }

    public String getName() {
        return name;
    }

    public CopyOnWriteArrayList<RoadLane> getRoadLanes() {
        return roadLanes;
    }

    public RailwayCrossing getRailwayCrossing() {
        return railwayCrossing;
    }

    public void setRailwayCrossing(RailwayCrossing railwayCrossing) {
        this.railwayCrossing = railwayCrossing;
    }

    public boolean isPositionRailwayCrossing(Position position) {
        for (Position p : this.railwayCrossing.getTrajectory()) {
            if (p.equals(position)) {
                return true;
            }
        }

        return false;
    }

    public void setSpeedLimit(int speedLimit) {
        this.speedLimit = speedLimit;
    }

    public int getSpeedLimit() {
        return this.speedLimit;
    }
}
