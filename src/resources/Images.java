package resources;

import javafx.scene.image.Image;
import models.dynamical.compositionables.Locomotive;

import java.util.Map;

public class Images {
    public static final Image CAR = new Image("resources/icons/yugo-23.jpg");
    public static final Image TRUCK = new Image("resources/icons/truck1.jpg");
    public static final Map<Locomotive.DriveType, Image> LOCOMOTIVE_IMAGES = Map.of(
            Locomotive.DriveType.STEAM, new Image("resources/icons/locomotive_steam.png"),
            Locomotive.DriveType.DIESEL, new Image("resources/icons/locomotive_diesel2.png"),
            Locomotive.DriveType.ELECTRIC, new Image("resources/icons/locomotive_electric3.png")
    );
    public static final Image SEAT_AND_BED_WAGON = new Image("resources/icons/wagon_1.jpeg");
    public static final Image RESTAURANT_WAGON = new Image("resources/icons/wagon_simple.jpeg");
    public static final Image CARGO_WAGON = new Image("resources/icons/wagon_cargo.png");
    public static final Image SPECIAL_WAGON = new Image("resources/icons/wagon_special.png");
}
